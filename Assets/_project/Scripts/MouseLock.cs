﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLock : MonoBehaviour {

    public bool lockOnStart = true;
    public bool relockOnClick = true;

    void Lock()
    {
        if (lockOnStart)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }
	// Use this for initialization
	void Start () {
        Lock();
	}
	
	// Update is called once per frame
	void Update () {
        if (relockOnClick && Input.GetMouseButtonDown(0))
        {
            Lock();
        }
	}
}
