﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class rotate : MonoBehaviour {

    public Rigidbody loneStar;
    public float speed;
    

	// Use this for initialization
	void Start () {
        loneStar = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(new Vector3(0,0,1) * Time.deltaTime * speed, Space.World);
	}
}
